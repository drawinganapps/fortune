import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortune/widgets/digital_card_widget.dart';
import 'package:fortune/widgets/top_up_balance_widget.dart';
import 'package:get/get.dart';

class TopUpPage extends StatelessWidget {
  const TopUpPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Get.height * 0.05),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(bottom: Get.height * 0.05),
            padding: EdgeInsets.only(
                left: Get.width * 0.05, right: Get.width * 0.05),
            child: const DigitalCardWidget(),
          ),
          const Expanded(child: TopUpBalanceWidget())
        ],
      ),
    );
  }
}
