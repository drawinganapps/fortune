import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fortune_wheel/flutter_fortune_wheel.dart';
import 'package:fortune/controller/fortune_controller.dart';
import 'package:fortune/data/dummy-data.dart';
import 'package:fortune/helper/color_helper.dart';
import 'package:fortune/widgets/lose_prize_widget.dart';
import 'package:fortune/widgets/win_prize_widget.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FortuneController>(builder: (controller) {
      var fortuneData = DummyData.fortuneData;
      return Container(
        padding: EdgeInsets.only(
          left: Get.width * 0.05,
          right: Get.width * 0.05,
        ),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(width: 1, color: ColorHelper.lightDarkSecondary),
            color: ColorHelper.lightDark.withOpacity(0.8)),
        child: FortuneWheel(
          animateFirst: false,
          onAnimationEnd: () {
            controller.setLoading(false);
            showDialog<String>(
              context: context,
              builder: (BuildContext context) => controller.fortunePrize.isLose
                  ? LosePrizeWidget(fortune: controller.fortunePrize)
                  : WinPrizeWidget(fortune: controller.fortunePrize),
            );
          },
          selected: controller.fortuneStream.stream,
          indicators: <FortuneIndicator>[
            FortuneIndicator(
              alignment: Alignment.topCenter,
              child: TriangleIndicator(
                color: ColorHelper.dark.withOpacity(0.8),
              ),
            ),
          ],
          items: List.generate(fortuneData.length, (index) {
            return FortuneItem(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: Get.width * 0.1),
                      child: Text(fortuneData[index].name,
                          style: GoogleFonts.combo(
                              color: ColorHelper.dark,
                              fontSize: 18,
                              fontWeight: FontWeight.w500)),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: Get.width * 0.01),
                      child: Image.asset(
                        'assets/images/${fortuneData[index].image}',
                        height: 50,
                        fit: BoxFit.cover,
                      ),
                    )
                  ],
                ),
                style: FortuneItemStyle(color: fortuneData[index].color));
          }),
        ),
      );
    });
  }
}
