import 'package:fortune/controller/balance_binding.dart';
import 'package:fortune/controller/fortune_binding.dart';
import 'package:fortune/screens/dashboard_screen.dart';
import 'package:fortune/screens/top_up_screen.dart';
import 'package:get/get.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.SPIN,
        page: () => const DashboardScreen(),
        bindings: [FortuneBinding(), BalanceBinging()]),
    GetPage(
        name: AppRoutes.TOP_UP,
        page: () => const TopUpScreen(),
        binding: BalanceBinging()),
  ];
}
