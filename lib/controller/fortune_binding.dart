import 'package:fortune/controller/fortune_controller.dart';
import 'package:get/get.dart';

class FortuneBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FortuneController>(() => FortuneController());
  }
}
