import 'package:fortune/controller/balance_controller.dart';
import 'package:get/get.dart';

class BalanceBinging extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BalanceController>(() => BalanceController());
  }

}