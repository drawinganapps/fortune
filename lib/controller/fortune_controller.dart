import 'dart:async';
import 'dart:math';
import 'package:fortune/data/dummy-data.dart';
import 'package:fortune/models/fortune-model.dart';
import 'package:get/get.dart';

class FortuneController extends GetxController {
  late StreamController<int> fortuneStream = StreamController.broadcast();
  late FortuneModel fortunePrize;
  bool isLoading = false;

  @override
  void onInit() {
    super.onInit();
  }

  void spinTheWheel() {
    var rangNumber = Random();
    var selectedNumber = rangNumber.nextInt(DummyData.fortuneData.length);
    fortuneStream.add(selectedNumber);
    fortunePrize = DummyData.fortuneData[selectedNumber];
    isLoading = true;
    update();
  }

  void setLoading(bool loading) {
    isLoading = loading;
    update();
  }

  void setFortunePrize(FortuneModel fortune) {
    fortunePrize = fortune;
  }
}
