import 'package:fortune/data/dummy-data.dart';
import 'package:fortune/models/payment_model.dart';
import 'package:get/get.dart';

class BalanceController extends GetxController {
  double _myBalance = 0.0;
  double _topUpAmount = 0.0;
  PaymentModel _selectedPayment = DummyData.payments[0];
  List<PaymentModel> _availablePayment = [];

  double get myBalance => _myBalance;

  double get topUpAmount => _topUpAmount;

  PaymentModel get selectedPayment => _selectedPayment;

  List<PaymentModel> get availablePayment => _availablePayment;

  @override
  void onInit() {
    super.onInit();
    _initDat();
  }

  void _initDat() {
    _myBalance = 100.0;
    _topUpAmount = 100.0;
    _selectedPayment = DummyData.payments[0];
    _availablePayment = DummyData.payments;
    update();
  }

  void setTopUpAmount(double amount) {
    _topUpAmount = amount;
    update();
  }

  void changePayment(PaymentModel payment) {
    _selectedPayment = payment;
    update();
  }

  void topUp() {
    _myBalance += _topUpAmount;
    update();
  }
}
