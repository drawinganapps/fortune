import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortune/controller/balance_controller.dart';
import 'package:fortune/helper/color_helper.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class DigitalCardWidget extends StatelessWidget {
  const DigitalCardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BalanceController>(builder: (controller) {
      return Container(
          width: Get.width,
          height: Get.height * 0.24,
          padding: EdgeInsets.only(
              top: Get.height * 0.015, bottom: Get.height * 0.015),
          decoration: BoxDecoration(
              color: ColorHelper.yellow,
              borderRadius: BorderRadius.circular(15)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.only(
                    left: Get.width * 0.05, right: Get.width * 0.05),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image.asset('assets/images/man.png',
                        height: Get.height * 0.05),
                    Image.asset('assets/images/visa.png',
                        height: Get.height * 0.05)
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: Get.width * 0.04,
                    bottom: Get.width * 0.04,
                    left: Get.width * 0.05,
                    right: Get.width * 0.05),
                decoration: BoxDecoration(color: Colors.black),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('1224',
                        style: GoogleFonts.roboto(
                            color: ColorHelper.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w600)),
                    Text('0000',
                        style: GoogleFonts.roboto(
                            color: ColorHelper.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w600)),
                    Text('7598',
                        style: GoogleFonts.roboto(
                            color: ColorHelper.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w600)),
                    Text('1112',
                        style: GoogleFonts.roboto(
                            color: ColorHelper.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w600)),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    left: Get.width * 0.05, right: Get.width * 0.05),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Digital Balance',
                        style: GoogleFonts.roboto(
                            fontSize: 16, fontWeight: FontWeight.w600)),
                    Text('\$ ${controller.myBalance.toStringAsFixed(2)}',
                        style: GoogleFonts.anton(
                            fontSize: 25, fontWeight: FontWeight.w200)),
                  ],
                ),
              )
            ],
          ));
    });
  }
}
