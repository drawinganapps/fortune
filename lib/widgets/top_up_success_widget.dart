import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortune/helper/color_helper.dart';
import 'package:fortune/models/fortune-model.dart';
import 'package:fortune/routes/AppRoutes.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class TopUpSuccessWidget extends StatelessWidget {
  final double topUpAmount;

  const TopUpSuccessWidget({super.key, required this.topUpAmount});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      backgroundColor: ColorHelper.yellow,
      title: Text('Payment Success!',
          style: GoogleFonts.roboto(fontWeight: FontWeight.w600, fontSize: 20)),
      content: SizedBox(
        height: Get.height * 0.2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: Get.height * 0.015),
              child: Text('Your payment has been received!',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.roboto(
                      fontWeight: FontWeight.w600, fontSize: 18)),
            ),
            Text(
                '\$ ${topUpAmount.toStringAsFixed(2)} has been added into your digital balance',
                textAlign: TextAlign.center,
                style: GoogleFonts.roboto(
                    fontWeight: FontWeight.w600, fontSize: 18)),
          ],
        ),
      ),
      actions: <Widget>[
        GestureDetector(
          onTap: () {
            Navigator.pop(context, 'OK');
            Get.toNamed(AppRoutes.SPIN);
          },
          child: Container(
            margin: EdgeInsets.only(bottom: Get.height * 0.01),
            padding: EdgeInsets.all(Get.width * 0.03),
            decoration: BoxDecoration(
                color: ColorHelper.primary,
                borderRadius: BorderRadius.circular(Get.width * 0.08)),
            child: Text('Play Now',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: ColorHelper.white)),
          ),
        )
      ],
    );
  }
}
