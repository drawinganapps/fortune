import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortune/controller/balance_controller.dart';
import 'package:fortune/helper/color_helper.dart';
import 'package:fortune/widgets/payment_option_widget.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

class TopUpBalanceWidget extends StatelessWidget {
  const TopUpBalanceWidget({super.key});


  @override
  Widget build(BuildContext context) {
    showMenuOption() {
      showModalBottomSheet(
          backgroundColor: Colors.transparent,
          context: context,
          builder: (BuildContext context) {
            return const FractionallySizedBox(
              heightFactor: 0.9,
              child: PaymentOptionWidget(),
            );
          });
    }

    return GetBuilder<BalanceController>(builder: (controller) {
      return Container(
        padding: EdgeInsets.only(
            left: Get.width * 0.05,
            right: Get.width * 0.05,
            top: Get.height * 0.015,
            bottom: Get.height * 0.015),
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                ColorHelper.lightDarkSecondary,
                ColorHelper.lightDark,
              ],
              end: Alignment.bottomCenter,
              begin: Alignment.topCenter,
            ),
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(35),
              topRight: Radius.circular(35),
            )),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
                padding: EdgeInsets.only(
                    top: Get.height * 0.025,
                    bottom: Get.height * 0.025,
                    left: Get.width * 0.03,
                    right: Get.width * 0.03),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: ColorHelper.whiteDarker.withOpacity(0.1)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                          left: Get.width * 0.015, right: Get.width * 0.015),
                      decoration: BoxDecoration(
                        color: ColorHelper.whiteDarker,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Image.asset('assets/images/${controller.selectedPayment.icon}',
                          height: Get.height * 0.05),
                    ),
                    Text(controller.selectedPayment.ownerName,
                        style: GoogleFonts.roboto(
                            color: ColorHelper.whiteDarker,
                            fontSize: 16,
                            fontWeight: FontWeight.w400)),
                    Text(controller.selectedPayment.cardNumber,
                        style: GoogleFonts.roboto(
                            color: ColorHelper.whiteDarker,
                            fontSize: 16,
                            fontWeight: FontWeight.w400)),
                    GestureDetector(
                      onTap: (){
                        showMenuOption();
                      },
                      child: Icon(Icons.autorenew, color: ColorHelper.whiteDarker),
                    )
                  ],
                )),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('\$ ${controller.topUpAmount.toStringAsFixed(2)}',
                    style: GoogleFonts.anton(
                        color: ColorHelper.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w200)),
                Container(
                  margin: EdgeInsets.only(bottom: Get.height * 0.03),
                  child: Text('Will be added in tou your digital balance.',
                      style: GoogleFonts.roboto(
                        fontSize: 18,
                        color: ColorHelper.white,
                      )),
                ),
                SfSliderTheme(
                    data: SfSliderThemeData(
                      activeLabelStyle:
                          TextStyle(color: ColorHelper.white, fontSize: 12),
                      inactiveLabelStyle: TextStyle(
                          color: ColorHelper.whiteDarker.withOpacity(0.8),
                          fontSize: 12),
                    ),
                    child: SfSlider(
                      min: 100,
                      max: 500,
                      interval: 100,
                      stepSize: 100,
                      showTicks: true,
                      value: controller.topUpAmount,
                      showLabels: true,
                      enableTooltip: true,
                      activeColor: ColorHelper.primary,
                      inactiveColor: ColorHelper.whiteDarker.withOpacity(0.5),
                      thumbShape: const SfThumbShape(),
                      labelFormatterCallback:
                          (dynamic actualValue, String formattedText) {
                        return '\$${actualValue.toStringAsFixed(0)}';
                      },
                      tooltipTextFormatterCallback:
                          (dynamic actualValue, String formattedText) {
                        return '\$ ${actualValue}';
                      },
                      onChanged: (dynamic value) {
                        controller.setTopUpAmount(value);
                      },
                    )),
              ],
            ),
          ],
        ),
      );
    });
  }
}
