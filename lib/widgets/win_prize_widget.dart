import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortune/helper/color_helper.dart';
import 'package:fortune/models/fortune-model.dart';
import 'package:get/get.dart';

class WinPrizeWidget extends StatelessWidget {
  final FortuneModel fortune;

  const WinPrizeWidget({super.key, required this.fortune});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      backgroundColor: ColorHelper.yellow,
      title: const Text('You Won!',
          style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20)),
      content: SizedBox(
        height: Get.height * 0.2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: Get.height * 0.015),
              child: Text('Congratulation You have won a ${fortune.name}',
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 18)),
            ),
            Image.asset(
              'assets/images/${fortune.image}',
              height: 100,
              fit: BoxFit.cover,
            ),
          ],
        ),
      ),
      actions: <Widget>[
        GestureDetector(
          onTap: () => Navigator.pop(context, 'OK'),
          child: Container(
            margin: EdgeInsets.only(bottom: Get.height * 0.01),
            padding: EdgeInsets.all(Get.width * 0.03),
            decoration: BoxDecoration(
                color: ColorHelper.primary,
                borderRadius: BorderRadius.circular(Get.width * 0.08)),
            child: Text('I Want More!',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: ColorHelper.white)),
          ),
        )
      ],
    );
  }
}
