import 'package:flutter/material.dart';
import 'package:fortune/controller/balance_controller.dart';
import 'package:fortune/helper/color_helper.dart';
import 'package:fortune/widgets/default_loading_widget.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loader_overlay/loader_overlay.dart';

class PaymentOptionWidget extends StatelessWidget {
  const PaymentOptionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BalanceController>(builder: (controller) {
      return Container(
        decoration: BoxDecoration(
          color: ColorHelper.lightDarkSecondary,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
                height: (56 * 6).toDouble(),
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                      color: ColorHelper.lightDarkSecondary,
                    ),
                    child: Stack(
                      alignment: const Alignment(0, 0),
                      children: <Widget>[
                        Positioned(
                          top: 10,
                          child: Center(
                            child: Text('Choose Payment:',
                                style: GoogleFonts.roboto(
                                    color: ColorHelper.whiteDarker,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w400)),
                          ),
                        ),
                        Positioned(
                          child: Container(
                            margin: EdgeInsets.only(top: Get.height * 0.05),
                            child: ListView(
                                physics: const NeverScrollableScrollPhysics(),
                                children: List.generate(
                                    controller.availablePayment.length,
                                    (index) {
                                  return GestureDetector(
                                    onTap: () {
                                      context.loaderOverlay.show(
                                          widget: const DefaultLoadingWidget());
                                      Future.delayed(const Duration(seconds: 1),
                                          () {
                                        controller.changePayment(
                                            controller.availablePayment[index]);
                                        context.loaderOverlay.hide();
                                      });
                                    },
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: Get.width * 0.05,
                                          right: Get.width * 0.05),
                                      margin: EdgeInsets.only(
                                          top: Get.height * 0.015),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: Get.width * 0.015,
                                                right: Get.width * 0.015),
                                            decoration: BoxDecoration(
                                              color: ColorHelper.whiteDarker,
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                            ),
                                            child: Image.asset(
                                              'assets/images/${controller.availablePayment[index].icon}',
                                              height: 50, fit: BoxFit.cover,
                                            ),
                                          ),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Center(
                                                child: Text(
                                                    controller
                                                        .availablePayment[index]
                                                        .name,
                                                    style: GoogleFonts.roboto(
                                                        color: ColorHelper
                                                            .whiteDarker,
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold)),
                                              ),
                                              Container(
                                                width: 20,
                                                height: 20,
                                                padding:
                                                    const EdgeInsets.all(1.5),
                                                margin: EdgeInsets.only(
                                                    left: Get.width * 0.05),
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    border: Border.all(
                                                        color:
                                                            ColorHelper.white)),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: controller
                                                                  .availablePayment[
                                                                      index]
                                                                  .id ==
                                                              controller
                                                                  .selectedPayment
                                                                  .id
                                                          ? ColorHelper.white
                                                          : ColorHelper
                                                              .lightDarkSecondary),
                                                ),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                })),
                          ),
                        )
                      ],
                    ))),
            Container(
              height: 90,
              color: ColorHelper.lightDarkSecondary,
              padding: EdgeInsets.only(bottom: Get.height * 0.035),
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Container(
                  width: Get.width * 0.60,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: ColorHelper.primary, width: 3)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Close',
                          style: GoogleFonts.roboto(
                              color: ColorHelper.primary,
                              fontSize: 18,
                              fontWeight: FontWeight.w600)),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      );
    });
  }
}
