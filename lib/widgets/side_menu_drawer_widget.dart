import 'package:flutter/material.dart';
import 'package:fortune/helper/color_helper.dart';
import 'package:fortune/routes/AppRoutes.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class SideMenuDrawerWidget extends StatelessWidget {
  const SideMenuDrawerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget generateMenuWidget(String title, bool isSelected) {
      return Container(
        padding: const EdgeInsets.only(left: 25, right: 10, top: 5, bottom: 5),
        width: 50,
        decoration: isSelected
            ? BoxDecoration(
                color: ColorHelper.primary.withOpacity(0.5),
                borderRadius:
                    const BorderRadius.only(bottomRight: Radius.circular(50)))
            : const BoxDecoration(),
        child: Text(title,
            style: GoogleFonts.roboto(
                fontSize: 22,
                color: ColorHelper.white,
                fontWeight: FontWeight.bold)),
      );
    }

    return Drawer(
      backgroundColor: ColorHelper.dark,
      child: Container(
        padding: const EdgeInsets.only(top: 55),
        child: Column(
          // Important: Remove any padding from the ListView.
          children: <Widget>[
            Container(
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
              ),
              clipBehavior: Clip.antiAlias,
              child: Image.asset(
                'assets/images/spin.gif',
                width: 50,
                height: 50,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 35),
              child: Text('Wheel of Fortune',
                  style: TextStyle(fontSize: 20, color: ColorHelper.white)),
            ),
            ListTile(
              title: generateMenuWidget(
                  'Spin', Get.currentRoute == AppRoutes.SPIN),
              onTap: () {
                Get.toNamed(AppRoutes.SPIN);
              },
            ),
            ListTile(
              title: generateMenuWidget(
                  'Top Up', Get.currentRoute == AppRoutes.TOP_UP),
              onTap: () {
                Get.toNamed(AppRoutes.TOP_UP);
              },
            ),
            ListTile(
              title: generateMenuWidget('About', false),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
