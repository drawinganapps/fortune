import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortune/controller/balance_controller.dart';
import 'package:fortune/helper/color_helper.dart';
import 'package:fortune/widgets/default_loading_widget.dart';
import 'package:fortune/widgets/top_up_success_widget.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loader_overlay/loader_overlay.dart';

class TopUpBottomNavigationWidget extends StatelessWidget {
  const TopUpBottomNavigationWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BalanceController>(builder: (controller) {
      return Container(
        height: Get.height * 0.12,
        padding: EdgeInsets.only(left: Get.width * 0.1, right: Get.width * 0.1),
        decoration: BoxDecoration(color: ColorHelper.lightDark),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () {
                context.loaderOverlay
                    .show(widget: const DefaultLoadingWidget());
                Future.delayed(const Duration(seconds: 1), () {
                  controller.topUp();
                  context.loaderOverlay.hide();
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) =>
                        TopUpSuccessWidget(topUpAmount: controller.topUpAmount),
                  );
                });
              },
              child: Container(
                width: Get.width * 0.80,
                height: Get.height * 0.08,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: ColorHelper.primary.withOpacity(0.8)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: Get.width * 0.05),
                      padding: EdgeInsets.all(Get.width * 0.02),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: ColorHelper.white)),
                      child: const Icon(Icons.payments, size: 30),
                    ),
                    Text('Top Up Now',
                        style: GoogleFonts.roboto(
                            color: ColorHelper.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400)),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
