import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortune/helper/color_helper.dart';
import 'package:fortune/models/fortune-model.dart';
import 'package:get/get.dart';

class LosePrizeWidget extends StatelessWidget {
  final FortuneModel fortune;

  const LosePrizeWidget({super.key, required this.fortune});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      backgroundColor: ColorHelper.primary,
      title: Text("Don't worry",
          style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 20,
              color: ColorHelper.whiteDarker)),
      content: SizedBox(
        height: Get.height * 0.2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: Get.height * 0.015),
              child: Text('Try it again, next spin The Fortune may be with you!',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: ColorHelper.whiteDarker,
                    fontSize: 20,
                  )),
            ),
            Image.asset(
              'assets/images/${fortune.image}',
              height: 65,
              width: 65,
              fit: BoxFit.fill,
            ),
          ],
        ),
      ),
      actions: <Widget>[
        GestureDetector(
          onTap: () => Navigator.pop(context, 'OK'),
          child: Container(
            margin: EdgeInsets.only(bottom: Get.height * 0.01),
            padding: EdgeInsets.all(Get.width * 0.03),
            decoration: BoxDecoration(
                border: Border.all(color: ColorHelper.yellow, width: 1),
                borderRadius: BorderRadius.circular(Get.width * 0.08)),
            child: Text('Spin it Again',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: ColorHelper.white)),
          ),
        )
      ],
    );
  }
}
