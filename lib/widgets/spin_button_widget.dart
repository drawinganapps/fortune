import 'package:flutter/cupertino.dart';
import 'package:fortune/controller/fortune_controller.dart';
import 'package:fortune/helper/color_helper.dart';
import 'package:get/get.dart';

class SpinButtonWidget extends StatelessWidget {
  const SpinButtonWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FortuneController>(builder: (controller) {
      return Container(
        height: Get.height * 0.15,
        padding: EdgeInsets.only(left: Get.width * 0.1, right: Get.width * 0.1),
        decoration: BoxDecoration(color: ColorHelper.dark),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () {
                controller.spinTheWheel();
              },
              child: controller.isLoading
                  ? Container()
                  : Container(
                width: Get.width * 0.80,
                height: Get.height * 0.08,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: ColorHelper.primary),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: Get.width * 0.05),
                      decoration:
                      const BoxDecoration(shape: BoxShape.circle),
                      clipBehavior: Clip.antiAlias,
                      child: Image.asset('assets/images/spin.gif',
                          height: 50),
                    ),
                    Text('Spin it Now',
                        style: TextStyle(
                            color: ColorHelper.white,
                            fontSize: 22,
                            fontWeight: FontWeight.w600)),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    });
  }

}
