import 'package:flutter/painting.dart';

class FortuneModel {
  int id;
  String name;
  Color color;
  bool isLose;
  String image;

  FortuneModel(this.id, this.name, this.color, this.image, this.isLose);
}
