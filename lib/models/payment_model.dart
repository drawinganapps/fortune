class PaymentModel {
  int id;
  String name;
  String icon;
  String ownerName;
  String cardNumber;

  PaymentModel(this.id, this.name, this.icon, this.ownerName, this.cardNumber);
}
