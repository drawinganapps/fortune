import 'package:flutter/material.dart';
import 'package:fortune/helper/color_helper.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData defaultTheme = ThemeData(
    brightness: Brightness.dark,
    scaffoldBackgroundColor: ColorHelper.dark,
    highlightColor: Colors.transparent,
    splashColor: Colors.transparent,
    primarySwatch: Colors.amber,
    textTheme: GoogleFonts.comboTextTheme().copyWith(
    ),
);
