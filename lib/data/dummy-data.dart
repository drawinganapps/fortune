import 'package:flutter/material.dart';
import 'package:fortune/models/fortune-model.dart';
import 'package:fortune/models/payment_model.dart';

class DummyData {
  static List<FortuneModel> fortuneData = [
    FortuneModel(9, 'JACKPOT', Colors.red, 'jackpot.png', false),
    FortuneModel(0, 'LOSE', Colors.green, 'trash.png', true),
    FortuneModel(1, '\$500', Colors.pinkAccent, 'money.png', false),
    FortuneModel(2, 'LOSE', Colors.blue, 'trash.png', true),
    FortuneModel(3, 'Iphone 14', Colors.yellow, 'iphone.png', false),
    FortuneModel(4, 'LOSE', Colors.purpleAccent, 'trash.png', true),
    FortuneModel(5, '\$45', Colors.deepOrange, 'money.png', false),
    FortuneModel(6, 'LOSE', Colors.indigoAccent, 'trash.png', true),
    FortuneModel(9, '\$25', Colors.limeAccent, 'money.png', false),
    FortuneModel(8, 'LOSE', Colors.white, 'trash.png', true),
  ];

  static List<PaymentModel> payments = [
    PaymentModel(1, 'Visa', 'visa.png', 'Ahmad Mirlan', '* **** 467'),
    PaymentModel(2, 'Master Card', 'mastercard.png', 'Ahmad Mirlan', '* **** 899'),
    PaymentModel(3, 'Paypal', 'paypal.png', '', 'a*****@mail.com'),
  ];

}
