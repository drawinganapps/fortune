import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortune/helper/color_helper.dart';
import 'package:fortune/pages/top_up_page.dart';
import 'package:fortune/widgets/side_menu_drawer_widget.dart';
import 'package:fortune/widgets/top_up_bottom_navigation_widget.dart';
import 'package:google_fonts/google_fonts.dart';

class TopUpScreen extends StatelessWidget {
  const TopUpScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorHelper.dark,
        title: Text('Top Up',
            style: GoogleFonts.anton(fontSize: 20, color: ColorHelper.white)),
        centerTitle: true,
        leading: Builder(builder: (context) {
          return IconButton(
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
            icon: Icon(Icons.menu, color: ColorHelper.white, size: 40),
          );
        }),
      ),
      body: const TopUpPage(),
      drawer: const SideMenuDrawerWidget(),
      bottomNavigationBar: const TopUpBottomNavigationWidget(),
    );
  }
}
