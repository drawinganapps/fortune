import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fortune/helper/color_helper.dart';
import 'package:fortune/pages/dashboard_page.dart';
import 'package:fortune/widgets/side_menu_drawer_widget.dart';
import 'package:fortune/widgets/spin_button_widget.dart';
import 'package:google_fonts/google_fonts.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorHelper.dark,
        title: Text('The Wheel of Fortune',
            style:
            GoogleFonts.anton(fontSize: 20, color: ColorHelper.white)),
        centerTitle: true,
        leading: Builder(builder: (context) {
          return IconButton(
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
            icon: Icon(Icons.menu, color: ColorHelper.white, size: 40),
          );
        }),
      ),
      drawer: const SideMenuDrawerWidget(),
      body: const DashboardPage(),
      bottomNavigationBar: const SpinButtonWidget(),
    );
  }
}
